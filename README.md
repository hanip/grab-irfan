# README #

This is the centralized monorepo for Irfan's Grab assignment test

### What is the structure of this repository? ###
This repository contains 3 main sections :	

* ####API####
	
	This is the main API created using Play Framework.
	
	Available http get routes to serve json :
	
	- /current : serving the current metric of supply-demand
	
	- /history : serving the history metric of supply-demand
	
	- /traffic : serving the history of traffic data
	
	for the list of supported parameters, kindly check `conf/routes`
	
	
* ####WWW####

	This folder contains python web server created using Flask microframework
	
	
* ####Data-Engineering, includes subfolder :####
	
	- #####azkaban-config#####
		- this folder contains azkaban configuration that stored the job commands to run applications under data-engineering
	
	- #####dummy-producer#####
		- this folder contains small python applications that produce dummy data to kafka
	
	- #####etl#####
		- this folder contains applications that used to do ETL, mostly using presto for data input and stored its output to the derived schema on hive
		
	- #####grab-flink#####
		- This is realtime consumer application for kafka. The application is provides the current metric for the system
	

### How do I get set up? ###

* #####API : #####

	- First you need sbt installed

	- For running server at local machine :
		- run `sbt run`
	
	- For generate docker image :
		- run `sbt docker:stage`
	
	- Jenkins deployer for production are available [here](http://ec2-52-64-228-255.ap-southeast-2.compute.amazonaws.com:8080/job/API/configure).

	
* #####WWW :#####

	- For running server :
		- run `python app.py`


* #####Data-Engineering :#####


	- #####azkaban-config#####
		- Azkaban server are installed on 52.62.148.249. updating configuration can be done by running bash script : `$ data-engineering/update-azkaban.sh` from the server.


	- #####grab-flink#####
	
		- First you need to run the flink server :
			- For example `flink-yarn-session -n 2`
	
		- Check your configuration (still hardcoded)
		
		- Then build this project to the fat-jar with :
			- `sbt assembly`
		
		- Run these two applications :
			- `flink run -c grab.irfan.consumers.BookingIncoming target/scala-2.11/grab-flink-assembly-1.0.jar`
			- `flink run -c grab.irfan.consumers.DriverLocation target/scala-2.11/grab-flink-assembly-1.0.jar`
		
		- Services are deployed on master machine on emr-cluster (54.66.154.106)
	
	
	- dummy-producer & etl runner command are in azkaban-config
	
	
	
### Who do I talk to? ###

If you have any questions, please don't hestitate to ask me questions at irfan.hanip@gmail.com