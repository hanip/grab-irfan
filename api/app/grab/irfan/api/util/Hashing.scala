package grab.irfan.api.util

import java.security.MessageDigest

object Hashing {
  def hashKey(input: String, algo: String = "SHA-256"): String = {
    // algo can be MD5, SHA-1, SHA-256
    val digest = MessageDigest.getInstance(algo)
    val hashedBytes = digest.digest(input.getBytes("UTF-8"))
    
    val stringBuffer = new StringBuffer()
    
    for (i <- 0 to hashedBytes.length - 1) {
        stringBuffer.append(Integer.toString((hashedBytes(i) & 0xff) + 0x100, 16).substring(1))
    }
    
    return stringBuffer.toString()
  }
}