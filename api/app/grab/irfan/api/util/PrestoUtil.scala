package grab.irfan.api.util

import java.sql.{Connection, DriverManager, ResultSet}
import java.util.Properties

import play.api.{Logger, Play}

object PrestoUtil{
  lazy val prestoUrl = Play.current.configuration.getString("presto.jdbcUrl").getOrElse("jdbc:presto://54.66.154.106:8889/hive")
  lazy val user = Play.current.configuration.getString("presto.user").getOrElse("root")
  lazy val password = Play.current.configuration.getString("presto.password")
  //lazy val awsCredentialsProviderClass = Play.current.configuration.getString("presto.credentialsClass").getOrElse("com.amazonaws.auth.DefaultAWSCredentialsProviderChain")

  def runQuery (query: String): (Connection, ResultSet) = {
    val info = new Properties()
    info.put("user", user)
    if (password.isDefined) {
      info.put("password", password)
    }
    //info.put("aws_credentials_provider_class", awsCredentialsProviderClass)

    val connection = DriverManager.getConnection(prestoUrl, info)

    Logger.logger.info(s"Run Query on Presto : ${query}")

    val statement = connection.createStatement

    val queryResults = statement.executeQuery(query)

    (connection, queryResults)
  }

}