package grab.irfan.api.modules

import java.util.concurrent.Executors

import com.redis.RedisClientPool
import grab.irfan.api.controllers.{DashboardApiController, HealthCheckApiController}
import grab.irfan.api.domains.supplydemand.{CachedPrestoSupplyDemandStorage, PrestoSupplyDemandStorage, RedisSupplyDemandStorage, SupplyDemandRepository, SupplyDemandService}
import grab.irfan.api.domains.traffic.{CachedPrestoTrafficStorage, PrestoTrafficStorage, TrafficRepository, TrafficService}
import grab.irfan.api.services.{RedisCacheService, RedisService}
import scaldi.Module

import scala.concurrent.ExecutionContext

class ApiModule extends Module {
  implicit lazy val executor = ExecutionContext.fromExecutor(
    Executors.newFixedThreadPool(inject[Int](identified by "application.maxThreadPoolCount")))

  //instantiate Redis
  lazy val redisServerUrl    = inject[String](identified by "redis.prestoCache.url")
  lazy val redisServerPort     = inject[Int](identified by "redis.prestoCache.port")
  lazy val redisClientPool = new RedisClientPool(redisServerUrl, 6379)
  lazy val redisCacheService  = new RedisService(redisClientPool)

  lazy val redisMetricUrl     = inject[String](identified by "redis.currentMetrics.url")
  lazy val redisMetricPort     = inject[Int](identified by "redis.currentMetrics.port")
  lazy val redisMetricClientPool = new RedisClientPool(redisMetricUrl, 6379)
  lazy val redisMetricService  = new RedisService(redisMetricClientPool)

  bind[RedisCacheService] identifiedBy 'prestoRedisCache to
    new RedisCacheService(redisCacheService)

  //supplyDemand domain
  bind[PrestoSupplyDemandStorage] identifiedBy 'prestoStorage to
    new PrestoSupplyDemandStorage()

  bind[SupplyDemandRepository] identifiedBy 'supplyDemandHistoryRepo to
    new CachedPrestoSupplyDemandStorage(
      prestoSupplyDemandStorage = inject[PrestoSupplyDemandStorage]('prestoStorage),
      redisCacheService = inject[RedisCacheService]('prestoRedisCache)
    )

  bind[SupplyDemandRepository] identifiedBy 'supplyDemandRealtimeRepo to
    new RedisSupplyDemandStorage(
      redisService = redisMetricService
    )

  bind[SupplyDemandService] identifiedBy 'supplyDemandService to
    new SupplyDemandService(
      historySupplyDemandRepository = inject[SupplyDemandRepository]('supplyDemandHistoryRepo),
      realtimeSupplyDemandRepository = inject[SupplyDemandRepository]('supplyDemandRealtimeRepo)
    )

  //traffic domain
  bind[PrestoTrafficStorage] identifiedBy 'prestoStorage to
    new PrestoTrafficStorage()

  bind[TrafficRepository] identifiedBy 'trafficHistoryRepo to
    new CachedPrestoTrafficStorage(
      prestoTrafficStorage = inject[PrestoTrafficStorage]('prestoStorage),
      redisCacheService = inject[RedisCacheService]('prestoRedisCache)
    )

  bind[TrafficService] identifiedBy 'trafficService to
    new TrafficService(
      historyTrafficRepository = inject[TrafficRepository]('trafficHistoryRepo)
    )
  
  //create main api
  binding to new DashboardApiController(
    supplyDemandService = inject[SupplyDemandService]('supplyDemandService),
    trafficService = inject[TrafficService]('trafficService))

  binding to new HealthCheckApiController()
}

