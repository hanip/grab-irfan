package grab.irfan.api

import grab.irfan.api.modules.ApiModule
import scaldi.play.ScaldiSupport

object Global extends play.api.GlobalSettings with ScaldiSupport {
 
  def applicationModule = new ApiModule
}
