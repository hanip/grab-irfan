package grab.irfan.api.domains.traffic

import grab.irfan.api.domains.geohash.GeoHash

case class Traffic(
  geoHash: GeoHash,
  avgSpeed: Double
) extends Serializable
