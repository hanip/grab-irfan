package grab.irfan.api.domains.traffic

import grab.irfan.api.domains.geohash.GeoHash
import org.joda.time.DateTime

import scala.concurrent.ExecutionContext

class TrafficService(
                      val historyTrafficRepository: TrafficRepository
                    )
                    (implicit executor: ExecutionContext) {
  def getHistoryTraffic(hash: GeoHash, time: DateTime) = {
    historyTrafficRepository.get(hash, time).map { result =>
      result.map { s =>
        val (lat, long) = GeoHash.decode(s.geoHash.id)
        new TrafficResponse(lat, long, getTrafficRating(s.avgSpeed))
      }
    }
  }

  private def getTrafficRating(avgSpeed: Double) = {
    20/(1+avgSpeed)
  }

}
