package grab.irfan.api.domains.traffic

case class TrafficResponse (
  lat: Double,
  long: Double,
  trafficRating: Double
) extends Serializable
