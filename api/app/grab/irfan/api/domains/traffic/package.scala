package grab.irfan.api.domains

import play.api.libs.json._

package object traffic {
  implicit val TrafficFormat = Json.format[Traffic]
  implicit val TrafficResponseFormat = Json.format[TrafficResponse]
}
