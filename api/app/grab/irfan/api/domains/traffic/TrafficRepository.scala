package grab.irfan.api.domains.traffic

import grab.irfan.api.domains.geohash.GeoHash
import grab.irfan.api.services.RedisCacheService
import grab.irfan.api.util.PrestoUtil
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import play.api.libs.json.Json

import scala.collection.mutable
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Success

trait TrafficRepository {

  def get(hash: GeoHash, dateEnd: DateTime): Future[List[Traffic]]

}

class CachedPrestoTrafficStorage(prestoTrafficStorage: PrestoTrafficStorage,
                           redisCacheService: RedisCacheService
)(implicit executor: ExecutionContext) extends TrafficRepository{
  private def getKey (geoHash: GeoHash, dateEnd: DateTime): String = {
    s"traffic:${geoHash.id}:${dateEnd.getMillis()}"
  }

  override def get(hash: GeoHash, dateEnd: DateTime): Future[List[Traffic]] = {
    val key = getKey(hash, dateEnd)

    getCachedValue(key).flatMap {
      case Some(p) =>
        Future(p)
      case _ => prestoTrafficStorage.get(hash, dateEnd)
        .andThen {
          case Success(p) => if (p.nonEmpty) {
            cacheResult(key, p)
          }
        }
    }
  }

  private def getCachedValue(cacheKey: String): Future[Option[List[Traffic]]] = {
    redisCacheService.get(cacheKey).map {
      case Some(jsonValue) => {
        Some(Json.parse(jsonValue).as[List[Traffic]])
      }
      case _ => {
        None
      }
    }
  }

  private def cacheResult(cacheKey: String, p: List[Traffic]) = {
    redisCacheService.set(cacheKey, Json.toJson(p).toString())
  }
}


class PrestoTrafficStorage()(implicit executor: ExecutionContext) {
  val partitionFormatter = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm")

  def get(geoHash: GeoHash, date: DateTime): Future[List[Traffic]] = Future {
    val strDate = partitionFormatter.print(date)

    val sql = s"""SELECT
         |  geohash,
         |  avg_speed
         |FROM
         |  derived.traffic
         |WHERE
         |  geohash like '${geoHash.id}%' AND
         |  hour like '${strDate.substring(0,13)}:00:00' """.stripMargin

    val (conn, rs) = PrestoUtil.runQuery(sql)

    val result = mutable.ListBuffer[Traffic]()
    while (rs.next()) {
      result += new Traffic(new GeoHash(rs.getString("geohash")), rs.getDouble("avg_speed"))
    }

    conn.close()

    result.toList
  }
}

