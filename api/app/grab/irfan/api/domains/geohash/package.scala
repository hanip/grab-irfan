package grab.irfan.api.domains

import play.api.libs.json.Json

package object geohash {
  implicit val GeoHashFormat = Json.format[GeoHash]
}
