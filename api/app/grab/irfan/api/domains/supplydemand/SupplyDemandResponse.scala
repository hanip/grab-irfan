package grab.irfan.api.domains.supplydemand

case class SupplyDemandResponse (
  lat: Double,
  long: Double,
  totalSupply: Int,
  totalDemand: Int,
  surgeRating: Double
) extends Serializable
