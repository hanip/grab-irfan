package grab.irfan.api.domains.supplydemand

import java.sql.ResultSet

import grab.irfan.api.domains.geohash.GeoHash
import grab.irfan.api.services.{RedisCacheService, RedisService}
import grab.irfan.api.util.PrestoUtil
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import play.api.libs.json.Json

import scala.collection.mutable
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration._
import scala.util.Success

trait SupplyDemandRepository {

  def get(hash: GeoHash, dateEnd: DateTime, minDuration: Int): Future[List[SupplyDemand]]

}

class CachedPrestoSupplyDemandStorage(prestoSupplyDemandStorage: PrestoSupplyDemandStorage,
                           redisCacheService: RedisCacheService
)(implicit executor: ExecutionContext) extends SupplyDemandRepository{
  private def getKey (geoHash: GeoHash, dateEnd: DateTime, minDuration: Int): String = {
    s"supplydemand:${geoHash.id}:${dateEnd.getMillis()}:${minDuration}"
  }

  override def get(hash: GeoHash, dateEnd: DateTime, minDuration: Int): Future[List[SupplyDemand]] = {
    val key = getKey(hash, dateEnd, minDuration)

    getCachedValue(key).flatMap {
      case Some(p) =>
        Future(p)
      case _ => prestoSupplyDemandStorage.get(hash, dateEnd, minDuration)
        .andThen {
          case Success(p) => if (p.nonEmpty) {
            cacheResult(key, p)
          }
        }
    }
  }

  private def getCachedValue(cacheKey: String): Future[Option[List[SupplyDemand]]] = {
    redisCacheService.get(cacheKey).map {
      case Some(jsonValue) => {
        Some(Json.parse(jsonValue).as[List[SupplyDemand]])
      }
      case _ => {
        None
      }
    }
  }

  private def cacheResult(cacheKey: String, p: List[SupplyDemand]) = {
    redisCacheService.set(cacheKey, Json.toJson(p).toString())
  }
}

class PrestoSupplyDemandStorage()(implicit executor: ExecutionContext) {
  val partitionFormatter = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm")

  def get(geoHash: GeoHash, dateEnd: DateTime, minDuration: Int): Future[List[SupplyDemand]] = Future {
    val (lat, long) = GeoHash.decodeAsBox(geoHash.id)

    val dateStart = dateEnd.minusMinutes(minDuration)

    val hours = (0 to ((minDuration + dateStart.getMinuteOfHour) / 60)).map(dateStart.plusHours(_))

    var sqlHours = mutable.ListBuffer[String]()

    for (hour <- hours) {
      val strHourStart = partitionFormatter.print(hour)
      val strHourEnd = partitionFormatter.print(dateEnd)

      if (hour == hours.head) {
        val filterFirstHour = s"""dt = '${strHourStart.substring(0, 10)}' AND hr = ${strHourStart.substring(11, 13).toInt} AND minute >= ${strHourStart.substring(14, 16).toInt}"""

        if (hour == hours.last) {
          sqlHours += s"(${filterFirstHour} AND minute < ${strHourEnd.substring(14, 16).toInt})"
        } else {
          sqlHours += s"(${filterFirstHour})"
        }
      } else if (hour == hours.last) {
        sqlHours += s"""(dt = '${strHourStart.substring(0, 10)}' AND hr = ${strHourStart.substring(11, 13).toInt} AND minute < ${strHourEnd.substring(14, 16).toInt})"""
      } else if (hour != hours.head) { //not first & last -> get all minute
        sqlHours += s"""(dt = '${strHourStart.substring(0, 10)}' AND hr = ${strHourStart.substring(11, 13).toInt})"""
      }
    }

    val sqlDate = sqlHours.mkString(" OR ")

    val sql = s"""SELECT
         |  rounded_lat, 
         |  rounded_long,
         |  nb_supply,
         |  nb_demand
         |FROM
         |  derived.supply_demand
         |WHERE
         |  rounded_lat >= ${lat._1} AND
         |  rounded_lat < ${lat._2} AND
         |  rounded_long >= ${long._1} AND
         |  rounded_long < ${long._2} AND
         |
         |${sqlDate}""".stripMargin

    val (conn, rs) = PrestoUtil.runQuery(sql)

    val result = toMapResult(rs)

    conn.close()

    result.values.toList
  }

  private def toMapResult(rs: ResultSet): Map[String, SupplyDemand] = {
    val result = mutable.HashMap[String, SupplyDemand]()

    while (rs.next()){
      val supply = rs.getInt("nb_supply")
      val demand = rs.getInt("nb_demand")

      val (lat, long) = (rs.getDouble("rounded_lat"), rs.getDouble("rounded_long"))
      val hash = GeoHash.encode(lat, long, 6)

      result.get(hash) match {
        case Some(existingMap) => {
          result.put(hash, new SupplyDemand(
            geoHash = existingMap.geoHash,
            nbSupply = existingMap.nbSupply + supply,
            nbDemand = existingMap.nbDemand + demand,
            0
          ))
        }
        case None => {
          result.put(hash, new SupplyDemand(
            geoHash = new GeoHash(hash),
            nbSupply = supply,
            nbDemand = demand,
            0
          ))
        }
      }
    }

    result.toMap
  }

}

class RedisSupplyDemandStorage(redisService: RedisService)(implicit executor: ExecutionContext) extends SupplyDemandRepository {

  def getRedisMetricSupply(supplyKey: String, tsStart: Long) = redisService.keys(s"${supplyKey}*").map {
    keys => {
      val result = mutable.HashMap[String, SupplyDemand]()

      val getRedisValue = keys.map {
        key => for {
          nbSupply <- redisService.zcountstartfrom(key, tsStart)
        } yield {
          val geoHash = key.substring(key.length - 6, key.length)
          result.put(geoHash, new SupplyDemand(
            geoHash = new GeoHash(geoHash),
            nbSupply = nbSupply.toInt,
            nbDemand = 0,
            0
          ))
        }
      }

      val async = Future.sequence(getRedisValue)
      Await.result(async, 5000 millis)
      result
    }
  }


  def getRedisMetricDemand(demandKey: String, tsStart: Long, resultMap: mutable.HashMap[String, SupplyDemand]) = redisService.keys(s"${demandKey}*").map {
    keys => {
      val getRedisValue = keys.map { key =>

        for {
          nbDemand <- redisService.zcountstartfrom(key, tsStart)
        } yield {
          val geoHash = key.substring(key.length - 6, key.length)

          resultMap.get(geoHash) match {
            case Some(existingMap) => {
              resultMap.put(geoHash, existingMap.copy(
                nbDemand = nbDemand.toInt
              ))
            }
            case None => {
              resultMap.put(geoHash, new SupplyDemand(
                geoHash = new GeoHash(geoHash),
                nbSupply = 0,
                nbDemand = nbDemand.toInt,
                0
              ))
            }
          }
        }
      }

      val async = Future.sequence(getRedisValue)
      Await.result(async, 5000 millis)
      resultMap
    }
  }

  override def get(hash: GeoHash, dateEnd: DateTime, minDuration: Int): Future[List[SupplyDemand]] = {
    val tsStart = dateEnd.minusMinutes(minDuration).getMillis
    val supplyKey = s"driver-location:${hash.id}"
    val demandKey = s"booking-incoming:${hash.id}"

    for {
      s <- getRedisMetricSupply(supplyKey, tsStart)
      result <- getRedisMetricDemand(demandKey, tsStart, s)
    } yield {
      result.values.toList
    }
  }
}

