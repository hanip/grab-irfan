package grab.irfan.api.domains.supplydemand

import grab.irfan.api.domains.geohash.GeoHash

case class SupplyDemand (
  geoHash: GeoHash,
  nbSupply: Int,
  nbDemand: Int,
  ratio: Double
) extends Serializable
