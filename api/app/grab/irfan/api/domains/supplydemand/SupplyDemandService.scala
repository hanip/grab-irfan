package grab.irfan.api.domains.supplydemand

import grab.irfan.api.domains.geohash.GeoHash
import org.joda.time.DateTime

import scala.concurrent.ExecutionContext

class SupplyDemandService(
                      val historySupplyDemandRepository: SupplyDemandRepository,
                      val realtimeSupplyDemandRepository: SupplyDemandRepository
                    )
                         (implicit executor: ExecutionContext) {
  def getHistorySupplyDemand(hash: GeoHash, time: DateTime, minDuration: Int) = {
    historySupplyDemandRepository.get(hash, time, minDuration).map { result =>
      result.map { s =>
        val (lat, long) = GeoHash.decode(s.geoHash.id)
        new SupplyDemandResponse(lat, long, s.nbSupply, s.nbDemand, getSurgeRating(s.nbSupply, s.nbDemand, minDuration))
      }
    }
  }

  def getCurrentSupplyDemand(hash: GeoHash, minDuration: Int) = {
    realtimeSupplyDemandRepository.get(hash, DateTime.now(), minDuration).map { result =>
      result.map { s =>
        val (lat, long) = GeoHash.decode(s.geoHash.id)
        new SupplyDemandResponse(lat, long, s.nbSupply, s.nbDemand, getSurgeRating(s.nbSupply, s.nbDemand, minDuration))
      }
    }
  }

  private def getSurgeRating(supply: Int, demand: Int, minDuration: Int) = {
    5 * 60/minDuration * Math.log(1 + (demand / Math.max(supply, 0.01)))
  }

}
