package grab.irfan.api.domains

import play.api.libs.json._

package object supplydemand {
  implicit val SupplyDemandFormat = Json.format[SupplyDemand]
  implicit val SupplyDemandResponseFormat = Json.format[SupplyDemandResponse]
}
