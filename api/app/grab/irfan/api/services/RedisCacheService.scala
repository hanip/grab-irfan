package grab.irfan.api.services



import scala.concurrent.{ExecutionContext, Future}

trait CacheService {

  def get(key: String): Future[Option[String]]

  def set(key: String, value: String, expirationTime: Int = 0): Future[Boolean]

  def del(key: String): Future[Boolean]

}

class RedisCacheService(redisService: RedisService)(implicit executor: ExecutionContext) extends CacheService {

  override def get(key: String): Future[Option[String]] = {
    redisService.get(key)
  }

  override def set(key: String, value: String, expirationTime: Int = 86400): Future[Boolean] = {
    redisService.set(key, value, expirationTime)
  }

  override def del(key: String): Future[Boolean] = {
    redisService.del(key)
  }

}
