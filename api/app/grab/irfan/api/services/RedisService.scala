package grab.irfan.api.services

import com.redis.RedisClientPool

import scala.concurrent.{ExecutionContext, Future, blocking}

class RedisService (connection: RedisClientPool) (implicit executor: ExecutionContext) {
  def get(key: String): Future[Option[String]] = Future {
    blocking {
      connection.withClient { client =>
        client.get(key)
      }
    }
  }

  def set(key: String, value: String, expirationTime: Int = 0): Future[Boolean] = Future {
    blocking {
      connection.withClient { client =>
        val isSet = client.set(key, value)

        if (expirationTime > 0)
          client.expire(key, expirationTime)

        isSet
      }
    }
  }

  def del(key: String): Future[Boolean] = Future {
    blocking {
      connection.withClient { client =>
        client.del(key) match {
          case None => false
          case _ => true
        }
      }
    }
  }

  def keys(pattern: String): Future[List[String]] = Future {
    blocking {
      connection.withClient { client =>
        val result = client.keys[String](pattern).map { keys =>
          keys.flatten
        }
        result.get
      }
    }
  }

  def zadd(key: String, score: Double, member: String): Future[Boolean] = Future {
    blocking {
      connection.withClient { client =>
        client.zadd(key, score, member) match {
          case None => false
          case _ => true
        }
      }
    }
  }

  def zremrangebyscore(key: String, start: Double, end: Double): Future[Long] = Future {
    blocking {
      connection.withClient { client =>
        client.zremrangebyscore(key, start, end) match {
          case None => 0L
          case Some(l) => l
        }
      }
    }
  }

  def zcountstartfrom(key: String, start: Double): Future[Long] = Future {
    blocking {
      connection.withClient { client =>
        client.zcount(key, start) match {
          case None => 0L
          case Some(l) => {
            l
          }
        }
      }
    }
  }
}