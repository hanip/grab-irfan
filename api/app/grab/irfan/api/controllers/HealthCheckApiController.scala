package grab.irfan.api.controllers

import play.api.libs.json.Json
import play.api.mvc.{Action, Controller}

class HealthCheckApiController extends Controller {

  def index = Action { implicit request =>
    Ok(Json.obj(
      "healthCheck" -> Json.obj(
        "status" -> "OK"
      )
    ))
  }
}
