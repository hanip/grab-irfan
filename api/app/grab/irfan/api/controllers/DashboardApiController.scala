package grab.irfan.api.controllers

import grab.irfan.api.domains.geohash.GeoHash
import grab.irfan.api.domains.supplydemand.SupplyDemandService
import grab.irfan.api.domains.traffic.TrafficService
import org.joda.time.DateTime
import play.api.libs.json._
import play.api.mvc._
import scaldi.{Injectable, Injector}

import scala.concurrent.{ExecutionContext, Future}

class DashboardApiController(supplyDemandService: SupplyDemandService,
                             trafficService: TrafficService)
                            (implicit inj: Injector, executor: ExecutionContext) extends Controller with Injectable {

  def getCurrentByHash(geoHash: String, minDuration: Int = 180) = Action.async(parse.empty) { implicit request =>
    val res = for {
      supplyDemandResponse <- supplyDemandService.getCurrentSupplyDemand(new GeoHash(geoHash), minDuration)
    } yield {
      Ok(Json.obj("supplydemand" -> Json.toJson(supplyDemandResponse))).withHeaders(
        ("Access-Control-Allow-Origin", "*")
      )
    }

    res.recoverWith {
      case e: Exception => {
        e.printStackTrace
        Future(NotFound)
      }
    }
  }


  def getCurrent(lat: Double, long: Double, minDuration: Int = 180) = Action.async(parse.empty) { implicit request =>
    val res = for {
      supplyDemandResponse <- supplyDemandService.getCurrentSupplyDemand(new GeoHash(GeoHash.encode(lat, long, 3)), minDuration)
    } yield {
      Ok(Json.obj("supplydemand" -> Json.toJson(supplyDemandResponse))).withHeaders(
        ("Access-Control-Allow-Origin", "*")
      )
    }

    res.recoverWith {
      case e: Exception => {
        e.printStackTrace
        Future(NotFound)
      }
    }
  }


  def getHistoryByHash(geoHash: String, endTimestamp: Int, minDuration: Int = 180) = Action.async(parse.empty) { implicit request =>
    val res = for {
      supplyDemandResponse <- supplyDemandService.getHistorySupplyDemand(new GeoHash(geoHash), new DateTime(endTimestamp.toLong * 1000), minDuration)
    } yield {
      Ok(Json.obj("supplydemand" -> Json.toJson(supplyDemandResponse))).withHeaders(
        ("Access-Control-Allow-Origin", "*")
      )
    }

    res.recoverWith {
      case e: Exception => {
        e.printStackTrace
        Future(NotFound)
      }
    }
  }


  def getHistory(lat: Double, long: Double, endTimestamp: Int, minDuration: Int = 180) = Action.async(parse.empty) { implicit request =>
    val res = for {
      supplyDemandResponse <- supplyDemandService.getHistorySupplyDemand(new GeoHash(GeoHash.encode(lat, long, 3)), new DateTime(endTimestamp.toLong * 1000), minDuration)
    } yield {
      Ok(Json.obj("supplydemand" -> Json.toJson(supplyDemandResponse))).withHeaders(
        ("Access-Control-Allow-Origin", "*")
      )
    }

    res.recoverWith {
      case e: Exception => {
        e.printStackTrace
        Future(NotFound)
      }
    }
  }


  def getTraffic(lat: Double, long: Double, endTimestamp: Int) = Action.async(parse.empty) { implicit request =>
    val res = for {
      trafficResponse <- trafficService.getHistoryTraffic(new GeoHash(GeoHash.encode(lat, long, 3)), new DateTime(endTimestamp.toLong * 1000))
    } yield {
      Ok(Json.obj("traffic" -> Json.toJson(trafficResponse))).withHeaders(
        ("Access-Control-Allow-Origin", "*")
      )
    }

    res.recoverWith {
      case e: Exception => {
        e.printStackTrace
        Future(NotFound)
      }
    }
  }
}
