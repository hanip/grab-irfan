import com.typesafe.sbt.packager.docker._

name := "api"
 
version := "latest"
      
scalaVersion := "2.11.11"

resolvers ++= Seq(
  "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases",
  "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"
)

enablePlugins(DockerPlugin)

lazy val root = (project in file(".")).enablePlugins(PlayScala)

lazy val scalaDependencies = Seq(
  "org.scaldi" %% "scaldi" % "0.5.6",
  "org.scaldi" %% "scaldi-play-23" % "0.5.6",
  "org.scaldi" %% "scaldi-akka" % "0.5.6",
  "org.scalatest" %% "scalatest" % "2.2.1" % "test",
  "net.debasishg" %% "redisclient" % "3.2"
  //"org.slf4j" % "log4j-over-slf4j" % "1.7.10" % "compile"
)

lazy val javaDependencies = Seq(
  "com.facebook.presto" % "presto-jdbc" % "0.184"
)

lazy val playDependencies = Seq(
  jdbc,
  cache,
  ws
)

libraryDependencies ++= scalaDependencies ++ javaDependencies ++ playDependencies

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.

// Disable ScalaDoc
sources in (Compile,doc) := Seq.empty
publishArtifact in (Compile, packageDoc) := false

// Docker Image Packaging
dockerBaseImage := "anapsix/alpine-java:jdk"
dockerExposedPorts := Seq(9000)
dockerCommands := dockerCommands.value.dropRight(3) // remove unused command
dockerCommands += ExecCmd("CMD", "sh", "-c", "bin/api -Dhttp.port=9000")
