from flask import Flask, render_template, request, redirect
import pymysql

db = pymysql.connect("localhost", "root", "password", "grab")
app = Flask(__name__)

@app.route("/current", methods=["GET"])
def current():
	long_value = request.args.get("long")
	lat_value = request.args.get("lat")
	return render_template('current.html', long_value = long_value, lat_value = lat_value)

@app.route("/history", methods=["GET"])
def history():
	long_value = request.args.get("long")
	lat_value = request.args.get("lat")
	date_value = request.args.get("date")
	return render_template('history.html', long_value = long_value, lat_value = lat_value, date_value = date_value)

@app.route("/traffic")
def traffic():
	long_value = request.args.get("long")
	lat_value = request.args.get("lat")
	date_value = request.args.get("date")
	return render_template('traffic.html', long_value = long_value, lat_value = lat_value, date_value = date_value)

@app.route("/weather")
def weather():
	#todo request with limit (pagination)
	cursor = db.cursor()

	cursor.execute("select location, `time`, `name`  from weather left join `condition` on weather.condition_id = `condition`.id;")

	return render_template('weather-table.html', data = cursor.fetchall())

@app.route("/")
def main():
	return redirect('/current?long=-74.025879&lat=40.763901')

if __name__ == "__main__":
	app.run()
