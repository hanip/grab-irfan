var map, pointarray, heatmap;
var currentStartDate = null;
var currentTimestamp = null;
var currentDuration = null;
//var apiUrl = "https://cors-anywhere.herokuapp.com/http://dashboard-api.ap-southeast-2.elasticbeanstalk.com/history?geo_hash=dr72";
var apiUrl = "http://localhost:9000/history"
var longValue = null;
var latValue = null;
var dateValue = null;

function initMap() {
    longValue = $('#long-value').val();
    latValue = $('#lat-value').val();

    var mapOptions = {
        zoom: 8,
        center: new google.maps.LatLng(latValue, longValue),
        mapTypeId: google.maps.MapTypeId.Map
    };

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    map.addListener("click", function (event) {
        latValue = event.latLng.lat();
        longValue = event.latLng.lng();

        console.log(latValue, longValue);
    });

    var heatMapData = [];

    var pointArray = new google.maps.MVCArray(heatMapData);

    heatmap = new google.maps.visualization.HeatmapLayer({
        data: pointArray,
        dissipating: true,
    });

    heatmap.setMap(map);
    
    setGradient();
    setLegendGradient();
    // updateFromDate();

    google.maps.event.addListenerOnce(map, 'tilesloaded', function() {
        $('#spinner').hide();
    });

}

function setGradient() {
    gradient = [
        'rgba(8, 255, 0, 0)',
        'rgba(8, 255, 0, 1)',
        'rgba(32, 255, 0, 1)',
        'rgba(56, 255, 0, 1)',
        'rgba(80, 255, 0, 1)',
        'rgba(104, 255, 0, 1)',
        'rgba(128, 255, 0, 1)',
        'rgba(151, 255, 0, 1)',
        'rgba(175, 255, 0, 1)',
        'rgba(199, 255, 0, 1)',
        'rgba(223, 255, 0, 1)',
        'rgba(247, 255, 0, 1)',
        'rgba(255, 239, 0, 1)',
        'rgba(255, 215, 0, 1)',
        'rgba(255, 191, 0, 1)',
        'rgba(255, 167, 0, 1)',
        'rgba(255, 143, 0, 1)',
        'rgba(255, 120, 0, 1)',
        'rgba(255, 96, 0, 1)',
        'rgba(255, 72, 0, 1)',
        'rgba(255, 48, 0, 1)',
        'rgba(255, 24, 0, 1)',
        'rgba(255, 0, 0, 1)'
    ]
    heatmap.set('gradient', gradient);
}

function setLegendGradient() {
    var gradientCss = '(left';
    for (var i = 0; i < gradient.length; ++i) {
        gradientCss += ', ' + gradient[i];
    }
    gradientCss += ')';
      
    $('#legendGradient').css('background', '-webkit-linear-gradient' + gradientCss);
    $('#legendGradient').css('background', '-moz-linear-gradient' + gradientCss);
    $('#legendGradient').css('background', '-o-linear-gradient' + gradientCss);
    $('#legendGradient').css('background', 'linear-gradient' + gradientCss);
}

function getUrl() {
    return apiUrl + "?long=" + longValue + "&lat=" + latValue + "&end_timestamp=" + currentTimestamp + "&tw_duration=" + currentDuration;
}
/*
function getUrl() {
    return apiUrl + "&end_timestamp=" + currentTimestamp + "&tw_duration=" + currentDuration;
}
*/

$(document).ready(function() {
    longValue = $('#long-value').val();
    latValue = $('#lat-value').val();

    currentDuration = $('#duration').val();

    if ($('#date-value').val() == null) {
        currentTimestamp = Math.floor(new Date().getTime() / 1000);
    } else {
        currentTimestamp = Math.floor(new Date($('#date-value').val()).getTime() / 1000);
    }
        
    strdate = new Date(currentTimestamp * 1000),
    strdateValues = [
       strdate.getFullYear(),
       strdate.getMonth()+1,
       strdate.getDate(),
       strdate.getHours(),
       strdate.getMinutes(),
       strdate.getSeconds(),
    ];

    $('#date-range').val(strdateValues[1] + "/" + strdateValues[2] + "/" + strdateValues[0]);

    var config = {
        separator: ' - ',
        singleDate: true,
        time: {
            enabled: true
        }
    };
    
    $(function () {
        $('#datetimepicker1').datetimepicker().on('dp.change', function (e) {
            currentTimestamp = parseInt(new Date(e.date).getTime() / 1000);
        });;
    });

    $('#duration').change(function (e) {
        currentDuration = $(this).val();
    });

});