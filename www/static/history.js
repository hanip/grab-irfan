//var apiUrl = "https://cors-anywhere.herokuapp.com/http://dashboard-api.ap-southeast-2.elasticbeanstalk.com/history?geo_hash=dr72";
var apiUrl = "http://localhost:9000/history"

function supplyDemand() {
    $('#spinner').show();
    var url = getUrl();

    $.get(url, function (response) {
        console.log(response);
        var data = response.supplydemand;
        $('#spinner').hide();
        var heatMapData = _.map(data, function (item) {
            return {
                location: new google.maps.LatLng(item.lat, item.long), 
                weight: item.surgeRating
            };
        });
        var pointArray = new google.maps.MVCArray(heatMapData); 
        heatmap.setData(pointArray);  

        setGradient();
        setLegendGradient();
        // setLegendLabels(true);  
    });
}

function getUrl() {
    return apiUrl + "?long=" + longValue + "&lat=" + latValue + "&end_timestamp=" + currentTimestamp + "&tw_duration=" + currentDuration;
}