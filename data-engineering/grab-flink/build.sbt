name := "grab-flink"

version := "1.0"

scalaVersion := "2.11.11"

libraryDependencies ++= {
  Seq(
    "org.apache.flink" %% "flink-connector-kafka-base" % "1.3.2",
    "org.apache.flink" %% "flink-connector-kafka-0.10" % "1.3.2",
    "org.apache.flink" %% "flink-streaming-scala" % "1.3.2",
    "net.debasishg" %% "redisclient" % "3.4",
    "com.fasterxml.jackson.core"    % "jackson-databind" % "2.8.4",
    "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.8.4",
    "log4j" % "log4j" % "1.2.17",
    "org.slf4j" % "slf4j-api" % "1.7.13" % "provided",
    "org.slf4j" % "slf4j-log4j12" % "1.7.13" % "provided"
  )
}

val meta = """META.INF(.)*""".r

assemblyMergeStrategy in assembly := {
  case "application.conf" => MergeStrategy.concat
  case meta(_) => MergeStrategy.discard

  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    //oldStrategy(x)
    MergeStrategy.first
}

mainClass in (Compile, run) := Some("grab.irfan.consumers.DriverLocation")