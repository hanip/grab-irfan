package grab.irfan.utils

import com.redis.RedisClientPool

object RedisUtil  {
  val connection: RedisClientPool = new RedisClientPool("172.31.26.237", 6379)

  def get(key: String): Option[String] =  {
    connection.withClient { client =>
      client.get(key)
    }
  }

  def set(key: String, expirationTime: Int): Boolean =  {
    connection.withClient { client =>
      val isSet = client.set(key, "1")

      if (expirationTime > 0)
        client.expire(key, expirationTime)
      isSet
    }
  }

  def del(key: String): Boolean =  {
    connection.withClient { client =>
      client.del(key) match {
        case None => false
        case _ => true
      }
    }
  }

  def zadd(key: String, score: Double, member: String): Boolean =  {
    connection.withClient { client =>
      client.zadd(key, score, member) match {
        case None => false
        case _ => true
      }
    }
  }

  def zremrangebyscore(key: String, start: Double, end: Double): Boolean = {
    connection.withClient { client =>
      client.zremrangebyscore(key, start, end) match {
        case None => false
        case _ => true
      }
    }
  }
}
