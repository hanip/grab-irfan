package grab.irfan.utils

import org.apache.flink.api.common.functions.AggregateFunction

import scala.collection.mutable.{Map => MutMap, Set => MutSet}

case class Accumulator (time: Long, geoHash: String, dataHash: MutSet[String])

class HashKeyAccumulator() extends AggregateFunction[(String, String), Accumulator, Accumulator]{
  override def createAccumulator(): Accumulator = {
    Accumulator(0L, "", MutSet[String]())
  }

  override def add(value: (String, String), acc: Accumulator): Unit = {
    val hash = value._2

    acc.dataHash += hash
  }

  override def merge(a: Accumulator, b: Accumulator): Accumulator = {
    a.dataHash ++= b.dataHash
    a
  }

  override def getResult(acc: Accumulator): Accumulator = {
    acc
  }
}
