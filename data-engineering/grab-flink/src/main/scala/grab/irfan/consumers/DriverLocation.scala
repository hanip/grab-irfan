package grab.irfan.consumers

import java.util.Properties

import com.fasterxml.jackson.databind.node.ObjectNode
import grab.irfan.Hash
import grab.irfan.utils.{Accumulator, GeoHash, HashKeyAccumulator, RedisUtil}
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.connectors.kafka._
import org.apache.flink.streaming.util.serialization.JSONDeserializationSchema
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.util.Collector

object DriverLocation {
  def main(args: Array[String]) = {

    val env = StreamExecutionEnvironment.getExecutionEnvironment

    val bootstrapServer = "54.153.141.241:9092,52.65.197.230:9092"
    val groupId = "grabreal_driver"
    val topic = "grab.driver-location"

    val properties = new Properties()
    properties.setProperty("bootstrap.servers", bootstrapServer)
    properties.setProperty("group.id", groupId)

    val stream = env.addSource(
      new FlinkKafkaConsumer010[ObjectNode](topic, new JSONDeserializationSchema(), properties))

    stream.map { m =>
      val lat = m.get("lat").asDouble()
      val long = m.get("long").asDouble()
      val driverId = m.get("driver_id").asInt()
      val timestamp = m.get("timestamp").asLong()
      val availability = m.get("is_available").asBoolean()

      val geoHash = GeoHash.encode(lat, long)
      val dataHash = Hash.hashKey(driverId.toString() + timestamp.toString())

      (geoHash, (dataHash, availability))
    }.filter(a => a._2._2)
      .map(v => (v._1, v._2._1))
      .keyBy(t => t._1)
      .timeWindow(Time.seconds(15)) //realtime data may off 15 sec + off from using consumer time.
      .aggregate(new HashKeyAccumulator(),
        ( key: String,
          window: TimeWindow,
          input: Iterable[Accumulator],
          out: Collector[Accumulator] ) => {
            var in = input.iterator.next()
            out.collect(Accumulator(window.getEnd, key, in.dataHash))
        }
      )
      .map {
        updatedLocation => updateRedis(updatedLocation)
      }

    env.execute("Driver Location Stream Processor")
  }


  def updateRedis(updatedLocation: Accumulator): Unit = {
    val redisKey = "driver-location:" + updatedLocation.geoHash

    //remove old data with 6 hour retention policy
    RedisUtil.zremrangebyscore(redisKey, 0, updatedLocation.time - 21600000)

    for (data <- updatedLocation.dataHash) {
      RedisUtil.zadd(redisKey, updatedLocation.time, data)
    }
  }

}
