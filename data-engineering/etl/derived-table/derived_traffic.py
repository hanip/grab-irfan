import shlex, subprocess
import Geohash
from datetime import datetime, timedelta
from pyspark import SparkContext, SparkConf
from pyspark.sql import Row, HiveContext

def _getopts(argv):
    opts = {}  # Empty dictionary to store key-value pairs.
    while argv:  # While there are arguments left to parse...
        if argv[0][0] == '-':  # Found a "-name value" pair.
            opts[argv[0]] = argv[1]  # Add key and value to the dictionary.
        argv = argv[1:]  # Reduce the argument list by copying it starting from index 1.
    return opts

def is_valid_row(row):
    try :
        pickup_dt = datetime.strptime(row[1], '%Y-%m-%d %H:%M:%S')
        dropoff_hour = datetime.strptime(row[2], '%Y-%m-%d %H:%M:%S')
        return (dropoff_hour - pickup_dt).total_seconds() > 0
    except:
        return False

def get_traffic_speed(row):
    pickup_dt = datetime.strptime(row[1], '%Y-%m-%d %H:%M:%S')
    dropoff_hour = datetime.strptime(row[2], '%Y-%m-%d %H:%M:%S')
    distance = float(row[0])
    time = (dropoff_hour - pickup_dt).total_seconds()

    return 3600 * distance/time

def find_geo_hash_between(lat1, long1, lat2, long2, geohash_carry):
    avg_lat = (lat1 + lat2) / 2
    avg_long =  (long1 + long2) / 2
    new_geohash = Geohash.encode(avg_lat, avg_long, 6)

    if (not(new_geohash in geohash_carry)):
        geohash_carry.append(new_geohash)
        find_geo_hash_between(avg_lat, avg_long, lat2, long2, geohash_carry)
        find_geo_hash_between(lat1, long1, avg_lat, avg_long, geohash_carry)

    return geohash_carry

def get_list_geohash_from_trip(row):
    geohash = []
    lat1 = float(row[4])
    long1 = float(row[3])
    geohash1 = Geohash.encode(lat1, long1, 6)
    geohash.append(geohash1)

    lat2 = float(row[6])
    long2 = float(row[5])
    geohash2 = Geohash.encode(lat2, long2, 6)

    if (geohash1 != geohash2) :
        geohash.append(geohash2)

    geohash = find_geo_hash_between(lat1, long1, lat2, long2, geohash)

    geohash_row = []
    for g in geohash:
        geohash_row.append((g, row))

    return geohash_row


def get_list_hour_from_trip(geohash_row):
    hours = []
    geohash = geohash_row[0]
    row = geohash_row[1]
    pickup_dt = datetime.strptime(row[1][:13], '%Y-%m-%d %H')
    dropoff_hour = datetime.strptime(row[2], '%Y-%m-%d %H:%M:%S')
    added_hour = 0

    while ((dropoff_hour - pickup_dt).total_seconds() > added_hour * 3600) :
        hour = pickup_dt + timedelta(hours=added_hour)
        hours.append('{:%Y-%m-%d %H:%M:%S}'.format(hour))
        added_hour = added_hour + 1

    hour_row = []
    for h in hours:
        hour_row.append((geohash, h, row))

    return hour_row


#command_line = "aws s3 rm s3://grab-irfan-data/derived/traffic --recursive"
command_line = "aws s3 rm s3://grab-irfan-data/raw/traffic.csv"
args = shlex.split(command_line)
p = subprocess.Popen(args)

conf = SparkConf().setAppName("Derived Traffic")
sc = SparkContext(conf = conf)
sqlContext = HiveContext(sc)

sqlDF = sqlContext.sql("select trip_distance, tpep_pickup_datetime, tpep_dropoff_datetime, pickup_longitude, pickup_latitude, dropoff_longitude, dropoff_latitude from raw.yellow_tripdata limit 1000")

#data cleansing
filtered_rdd = sqlDF.rdd.repartition(1000).filter(lambda row: is_valid_row(row) and (float(row[3]) < -65 and float(row[3]) > -80 and float(row[5]) < -65 and float(row[5]) > -80) and (float(row[4]) < 50 and float(row[4]) > 30 and float(row[6]) < 50 and float(row[6]) > 30))

list_geohash_rdd = filtered_rdd.flatMap(lambda row: get_list_geohash_from_trip(row))
list_geohash_hour_rdd = list_geohash_rdd.flatMap(lambda row: get_list_hour_from_trip(row))
prepared_rdd = list_geohash_hour_rdd.map(lambda kv:((kv[0], kv[1]), (kv[2]))).repartition(24)

aggregated_rdd = prepared_rdd.mapValues(lambda x: (get_traffic_speed(x), 1)).reduceByKey(lambda x, y: (x[0] + y[0], x[1] + y[1])).mapValues(lambda x: x[0]/x[1])

traffic = aggregated_rdd.map(lambda x: Row(geohash=x[0][0], hour=x[0][1], avg_speed=x[1]))
schemaTraffic = sqlContext.createDataFrame(traffic)

#schemaTraffic.write.parquet("s3n://grab-irfan-data/derived/traffic", mode="append")
schemaTraffic.repartition(1).write.csv("s3n://grab-irfan-data/raw/traffic.csv", mode="append")