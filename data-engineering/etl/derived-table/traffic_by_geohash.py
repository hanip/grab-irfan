import prestodb
import Geohash
from datetime import datetime, timedelta

def _getopts(argv):
    opts = {}  # Empty dictionary to store key-value pairs.
    while argv:  # While there are arguments left to parse...
        if argv[0][0] == '-':  # Found a "-name value" pair.
            opts[argv[0]] = argv[1]  # Add key and value to the dictionary.
        argv = argv[1:]  # Reduce the argument list by copying it starting from index 1.
    return opts

def is_valid_row(row):
    pickup_dt = datetime.strptime(row[1], '%Y-%m-%d %H:%M:%S')
    dropoff_hour = datetime.strptime(row[2], '%Y-%m-%d %H:%M:%S')
    return (dropoff_hour - pickup_dt).total_seconds() > 0

def get_traffic_speed(row):
    pickup_dt = datetime.strptime(row[1], '%Y-%m-%d %H:%M:%S')
    dropoff_hour = datetime.strptime(row[2], '%Y-%m-%d %H:%M:%S')
    distance = float(row[4])
    time = (dropoff_hour - pickup_dt).total_seconds()

    return 3600 * distance/time

def find_geo_hash_between(lat1, long1, lat2, long2, geohash_carry):
    avg_lat = (lat1 + lat2) / 2
    avg_long =  (long1 + long2) / 2
    new_geohash = Geohash.encode(avg_lat, avg_long, 6)

    if (not(new_geohash in geohash_carry)):
        geohash_carry.append(new_geohash)
        find_geo_hash_between(avg_lat, avg_long, lat2, long2, geohash_carry)
        find_geo_hash_between(lat1, long1, avg_lat, avg_long, geohash_carry)

    return geohash_carry

def get_list_geohash_from_trip(row):
    geohash = []
    lat1 = float(row[6])
    long1 = float(row[5])
    geohash1 = Geohash.encode(lat1, long1, 6)
    geohash.append(geohash1)

    lat2 = float(row[10])
    long2 = float(row[9])
    geohash2 = Geohash.encode(lat2, long2, 6)

    if (geohash1 != geohash2) :
        geohash.append(geohash2)

    geohash = find_geo_hash_between(lat1, long1, lat2, long2, geohash)

    return geohash

def get_list_hour_from_trip(row):
    hours = []
    pickup_dt = datetime.strptime(row[1][:13], '%Y-%m-%d %H')
    dropoff_hour = datetime.strptime(row[2], '%Y-%m-%d %H:%M:%S')
    added_hour = 0

    while ((dropoff_hour - pickup_dt).total_seconds() > added_hour * 3600) :
        hour = pickup_dt + timedelta(hours=added_hour)
        hours.append('{:%Y-%m-%d %H:%M:%S}'.format(hour))
        added_hour = added_hour + 1

    return hours


if __name__ == '__main__':
    outputfile = open('/home/ubuntu/data-staging/output.txt', 'wb')

    conn=prestodb.dbapi.connect(
        host='172.31.35.130',
        port=8889,
        user='root',
        catalog='hive',
        schema='default'
    )

    cur = conn.cursor()

    query = """ select * from raw.yellow_tripdata where (tpep_pickup_datetime like '2016-06-08%' or tpep_pickup_datetime like '2016-06-09%') and tpep_pickup_datetime like '%0'"""
    cur.arraysize = 100
    skipHeader = True
    print ("Run ETL Query : " + query)
    cur.execute(query)

    count = 0
    readCount = 0

    while True:
        row = cur.fetchone()
        readCount = readCount + 1
        
        if (row == None):
            break

        if (count % 10000 == 0):
            print ("nb line read: " + str(readCount))
            
        if (is_valid_row(row)):
            for hour in get_list_hour_from_trip(row):
                for geohash in get_list_geohash_from_trip(row):
                    outputfile.write("{hourtime}, {geohash}, {speed} \n".format(hourtime = hour, geohash = geohash, speed = get_traffic_speed(row)))
                    count = count + 1
                    if (count % 10000 == 0):
                        print ("nb line written: " + str(count))


    print ("nb line read: " + str(readCount))
    print ("nb line written: " + str(count))
