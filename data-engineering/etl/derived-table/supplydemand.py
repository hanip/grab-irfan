import shlex, subprocess
from pyhive import hive
from datetime import datetime, timedelta

process_hour_date = datetime.now() - timedelta(hours = 2)
str_hour_date = process_hour_date.strftime('%Y-%m-%d %H')

date = str_hour_date[:10]
hour = str_hour_date[-2:]

print ("process ETL supply_demand, with time arguments = %s" % str_hour_date)

#Setup hive connection
hive_conn = hive.connect(
    host='172.31.3.98',
    port=10000
)

hive_cur = hive_conn.cursor()
query = "msck repair table raw.driver_location"
print("Run fix partition query : " + query)
hive_cur.execute(query)

hive_cur = hive_conn.cursor()
query = "msck repair table raw.booking_incoming"
print("Run fix partition query : " + query)
hive_cur.execute(query)

#Setup presto connection
query = """
    insert into
        derived.supply_demand
    with sd as (
        select round(lat, 3) rnd_lat,
        round(long,3) rnd_long,
        (timestamp/60000) % 60 minute,
        count() nb_supply,
        0 nb_demand
    from
        raw.driver_location
    where
        is_available and
        dt = '{date}' and
        hr = {hour}
    group by
        1,2,3
    union all
    select
        round(lat, 3) rnd_lat,
        round (long, 3) rnd_long,
        (timestamp/60000) % 60 minute,
        0 nb_supply,
        count() nb_demand
    from
        raw.booking_incoming
    where
        dt = '{date}' and
        hr = {hour}
    group by 1,2,3)

    select
        rnd_lat,
        rnd_long,
        cast(minute as int),
        cast(sum(nb_supply) as int),
        cast (sum(nb_demand) as int),
        '{date}',
        {hour}
    from
        sd
    group by 1,2,3
    """.format(date = date, hour = int(hour))

print ("Run ETL Query : " + query)

command_line = "/home/ubuntu/presto --server 172.31.35.130:8889 --catalog hive --schema default --execute \"" + query.replace('\n', ' ').replace('\r', '') + "\""
args = shlex.split(command_line)
p = subprocess.Popen(args)

hive_cur = hive_conn.cursor()
query = "msck repair table derived.supply_demand"
print("Run fix partition query : " + query)
hive_cur.execute(query)
