import kafka
import json
import datetime
import time
import random

kafka_servers = ['54.153.141.241:9092','52.65.197.230:9092']

kafka_topic = 'grab.booking-incoming'
kafka_producers = kafka.KafkaProducer(
    bootstrap_servers = kafka_servers,
    value_serializer=lambda m: json.dumps(m).encode('ascii'),
    request_timeout_ms=5000,
    max_block_ms=5000
)

print("Finished create producer")

for x in range(0, 120):
	book_timestamp = int(round( time.time() * 1000))
	book_time = datetime.datetime.utcfromtimestamp(int(str(book_timestamp)[:10])).strftime('%Y-%m-%d %H:%M:%S')
	cust_id = random.randint(0,1000)
	book_long = -73.778889 + (cust_id % 7) * 0.03 + (book_timestamp % 10000) * 0.0001
	book_lat = 40.639722 + (cust_id % 7) * 0.03 + (book_timestamp % 10000) * 0.0001

	kafka_data = {'timestamp': book_timestamp, 'cust_id': cust_id, 'long': book_long, 'lat': book_lat}
	future = kafka_producers.send(topic=kafka_topic, value=kafka_data, timestamp_ms=book_timestamp)
	result = future.get(timeout=60)
	time.sleep(30)

print("FINISH")
