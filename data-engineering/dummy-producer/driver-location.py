import kafka
import json
import datetime
import time
import random

kafka_servers = ['54.153.141.241:9092','52.65.197.230:9092']

kafka_topic = 'grab.driver-location'
kafka_producers = kafka.KafkaProducer(
    bootstrap_servers = kafka_servers,
    value_serializer=lambda m: json.dumps(m).encode('ascii'),
    request_timeout_ms=5000,
    max_block_ms=5000
)

print("Finished create producer")

for x in range(0, 720):
	driver_timestamp = int(round( time.time() * 1000))
	driver_time = datetime.datetime.utcfromtimestamp(int(str(driver_timestamp)[:10])).strftime('%Y-%m-%d %H:%M:%S')
	driver_id = random.randint(0,25)
	driver_long = -73.778889 + (driver_id % 7) * 0.03 + (driver_timestamp % 10000) * 0.0001
	driver_lat = 40.639722 + (driver_id % 7) * 0.03 + (driver_timestamp % 10000) * 0.0001
	random_availability = random.randint(0, driver_id * 40)
	driver_availability = random_availability > (x - 400)

	kafka_data = {'timestamp': driver_timestamp, 'driver_id': driver_id, 'long': driver_long, 'lat': driver_lat, 'is_available': driver_availability}
	future = kafka_producers.send(topic=kafka_topic, value=kafka_data, timestamp_ms=driver_timestamp)
	result = future.get(timeout=60)
	time.sleep(5)

print("FINISH")
